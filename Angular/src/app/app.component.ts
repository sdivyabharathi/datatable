import { Component,OnInit,ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {UserService } from './user.service'
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import  *  as  data  from  '../assets/user.json';
export interface UsersData {
  _id:number,
  name: string,
  email:string,
  role:string;
  created_by:string,
  created_at:number
}
const ELEMENT_DATA: UsersData[] = [];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public Users:any=data;
  displayedColumns: string[] = ['Name','Email','Role','Created_by','Created_at', 'Option'];
  dataSource:MatTableDataSource<UsersData>;
  @ViewChild(MatSort,{static: false}) sort!: MatSort;
  @ViewChild(MatPaginator)paginator!: MatPaginator; 
  
  constructor(public dialog: MatDialog,private service:UserService) {
    this.dataSource = new MatTableDataSource(); 
    
    
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  ngOnInit() {
  this.getRowData();
  
  }
  openDialog(action:any,obj:any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });
  dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
  });}
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  getRowData(){
  this.service.findAll().subscribe((data:any)=>{
    this.dataSource.data= data;
  }); }

  addRowData(row_obj:any){
    var d = new Date();
    let value={
      _id:row_obj._id,
      created_at:d.getTime(),
      name:row_obj.name,
      email:row_obj.email,
      role:row_obj.role,
      created_by:row_obj.created_by
    }
    this.dataSource.data.push(value)
    this.service.create(value).subscribe((data:any)=>{
      this.getRowData();
    });}

  updateRowData(row_obj:any){
    this.dataSource.data = this.dataSource.data.filter((value,key)=>{
      if(row_obj._id==value._id){
        value.name = row_obj.name;
        value.email=row_obj.email,
        value.role=row_obj.role,
        value.created_by=row_obj.created_by
       this.service.update(row_obj._id,value).subscribe((data:any)=>{
      });}
      return true;
      });}
  
  deleteRowData(row_obj:any){
    this.service.delete(row_obj._id).subscribe((data:any)=>{
        this.getRowData();
      });}
}





