import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import {  throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from "../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class UserService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private httpClient: HttpClient) { }
  create(body:any){
    return this.httpClient.post(`${environment.apiUrl}/app/create`, JSON.stringify(body), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  findAll(){
    return this.httpClient.get(`${environment.apiUrl}/app/findAll`, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  } 
  update(id:number, body:any) {
    return this.httpClient.put(`${environment.apiUrl}/app/update/${id}`, JSON.stringify(body), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  delete(id:number){
    return this.httpClient.delete(`${environment.apiUrl}/app/delete/${id}`,this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  errorHandler(error:any) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
  }
}
