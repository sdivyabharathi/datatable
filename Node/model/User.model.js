const mongoose = require("../config/database");

var userSchema = new mongoose.Schema({
    id:Number,
    name:String,
    email:String,
    role:String,
    created_by: String,
    created_at:String,
   });
const collectionName = "userCollections"; // Name of the collection of documents
const userModel = mongoose.model(collectionName, userSchema);

module.exports = userModel;