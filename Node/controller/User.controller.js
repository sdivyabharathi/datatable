const userModel=require("../model/User.model");

exports.create = (req, res,next) => {
    let person = new userModel(
        {   _id:req.body._id,
            name: req.body.name,
            email: req.body.email,
            role:req.body.role,
            created_by:req.body.created_by,
            created_at:req.body.created_at
        });
        person.save( (err,data) =>{
        if (err) {
            return next(err);
        }
        res.status(200).json(data)
        })};
exports.findAll = (req, res,next) => {
    userModel.find((err, result)=> {
        // if we have an error
        if (err) {
            res.status(400).send('error', { error: err });
        }
        else {
         res.status(200).json(result)
        }
    });}; 

exports.update = (req, res,next) => {
    userModel.findByIdAndUpdate(req.params.id, {$set: req.body}, (err, result)=> {
            if (err){
             return next(err);
            }
            else{
            res.status(200).json(result);
            }
        });};
exports.delete = (req, res, next) => {
    userModel.findByIdAndRemove(req.params.id,  (err) =>{
        if (err) throw(err);
        res.json({result:'Deleted User detail succesfully'})
    })};