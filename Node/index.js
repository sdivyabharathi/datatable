const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const userRoute=require("./routes/User.route");
app.use(cors());
app.use((req,res,next)=>{
    res.setHeader('Content-Type', 'application/json');
    res.set('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Orgin',"*");
    res.header("Access-Control-Allow-Header","Orgin,X-Requested-with,Content-Type,Accept,Authorization");
    
    next();
    }); 
    // for parsing application/xwww-
app.use(express.urlencoded({ extended: false }));
// for parsing application/json
app.use(express.json());


//form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
 // parse application/json
app.use(bodyParser.json())

//initialize app
app.use('/app',userRoute);
app.use((req,res,next)=>{
    const error = new Error('url not found');
    error.status=404;
    next(error);
    });

module.exports = app;