const express=require('express');
const router =express.Router();
const userController=require('../controller/User.controller');

router.post('/create', userController.create);
router.get('/findAll', userController.findAll);
router.put('/update/:id', userController.update);
router.delete('/delete/:id', userController.delete);

module.exports=router;